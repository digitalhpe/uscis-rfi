#!/bin/bash

## Compile and package steps only belong in this script.
## The script must return proper return codes so that when chained with other steps
## in the build lifecycle, other scripts in the sequence will know whether to continue to process or not.

echo "Running tests for Admin Web Site"

cd admin-web

npm run test

if [ $? -eq 0 ]
then
	echo "Successfully tested the app."
	exit 0
else
	echo "Tests Failed."
	exit 1
fi
