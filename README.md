# DHS RFI 70SBUR18I00000004 #

This repository contains the response to question 10 from the RFI.

## Functioning Software ##
The two sites can be found at:

* [Photo Upload Site](https://uscisupload.digitaldxc.com)
* [Administrative Site](https://uscisadmin.digitaldxc.com)

### Upload Site ###
The upload site allows a user to upload a photo or take a new photo with their webcam or mobile device camera. The user also enters a code to go along with the photo. The codes are single-use, and associated with already existing users in the system. The Admin site can be used to reset the codes for additional demonstrations.

**Avaialable Codes**

There are 20 free codes to use for testing. The available codes are in the range:
> 
> ABC100 - ABC119
>

So when a user uploads a photo, they need to enter a code, such as *ABC101*. If a code is used and a user tries it again (or another user tries the code) they will get an error message that the code has been used. Just choose another code until the codes are used up. If the codes get used up, there is a button on the Status page of the Admin Site that allows you to reset the data to the inital state to begin the demo over.

### Admin Site ###
The admin site allows users to browse pending photo submissions, and approve or deny those submissions. Additionally, users can see and search through a list of all submissions.

**UserName**: 
photo-admin

**Password**:
approver

## Technical Details ##
The backend of the upload site and API are built using Node.js. Node was chosen because it is a good platform for rapid prototyping, and the compressed timeframe to build these applications justified that choice. The database used by the upload site is MongoDB. [The APIs are documented in this repository](API.html).

The admin site is built using Vue.js and only operates in the client-side browser. There are no server-side components to the application save the web server that is necessary to host the static files. The admin site only interacts with APIs hosted in the upload site.

Each of the three components (upload site, admin site, database) are hosted in Docker containers. Docker Compose is used to instantiate the containers.

### Building the Applications ###

The applications can be built and run locally on Mac OS or Linux (or in the Linux subsytem for Windows in Windows 10). Docker must be installed locally.

After making all the shell files in the root executable, execute *build.sh*. The script will build the software, run tests, download the necessary Docker containers, create and then run application instances using Docker Compose. The sites will be available locally on ports 8005 (upload site) and 8006 (admin site)

### Continuous Delivery ###
In our development environment, code checked into BitBucket is automatically built, tested and deployed by Jenkins.

![Jenkins Job](img/jenkins-1.png)

Our Jenkins job configuration is minimal. The work of building the software is encapsulated in the shell scripts in the root of the project. This allows the project build to work locally and become portable to other build servers.

This response used a pre-existing development infrastructure to support the development team:

![Infrastructure diagram](img/cicd.png)

### Workflow ###
The development team followed this workflow:

![Developer Proxcess](img/DevelopmentProcess.png)

The development team uses Jira to track their workflow. We created stories and assigned them to development staff. The state of the project was captured in images and can be found in the *img/jira folder* of this repository. 
#### Design ####
The design artifacts for this project are located in the *img/design* folder of this repository. Due to the small size and compressed duration of the project the team skipped our traditional step of wireframing and went straight on to mockups. Mockups and style guides are included.