const passport = require("passport");
const passportJWT = require("passport-jwt");
const config = require('config');
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;
const params = {
    secretOrKey: config.get('secret'),
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};
const db = require('../data/data');

module.exports = function() {
    const strategy = new JwtStrategy(params, function(payload, done) {
        db.getAdminUser(payload.id)
            .then(function(user) {
                if (user) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            })
            .catch(function(err){
                return done(err, false);
            });
    });
    passport.use(strategy);
    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function() {
            return passport.authenticate("jwt", { session: false });
        }
    };
};