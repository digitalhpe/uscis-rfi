"use strict";

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let userSchema = new Schema({
    code: {type: String, unique: true, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    birthDate: String,
    address: String,
    image: String,
    imageType: String,
    status: String,
    comment: String,
    updateDate: {type: Date}

}, { collection : 'user' });

    let user = mongoose.model('user', userSchema);

module.exports = user;
