"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');
const bcrypt = require('bcrypt-nodejs');

const adminUserSchema = new Schema({
    username: String,
    password: String
}, { collection : 'adminUser' });

adminUserSchema.pre('save', function (next) {
    const user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

adminUserSchema.methods.comparePassword = function (pass, cb) {
    console.log(this.password);
    console.log(pass);
    bcrypt.compare(pass, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

adminUserSchema.plugin(passportLocalMongoose);

let adminUser = mongoose.model('adminUser', adminUserSchema);

module.exports = adminUser;
