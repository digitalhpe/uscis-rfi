
"use strict";

let mongoose = require('mongoose');
var config = require('config');
let User = require('../models/user');

let data = module.exports = {};

function createUser(imageType, image, code) {
    let item = new User();
    item.imageType = imageType;
    item.image = image;
    item.code = code;
    return item;
}

function createUserForAdd(code, image, imageType) {
    let item = new User();
    item.code = code;
    item.image = image;
    item.imageType = imageType;
    return item;
}

function createUserForUpdate(code, status, comment) {
    let item = new User();
    item.code = code;
    item.status = status;
    item.comment = comment;
    return item;
}

data.addImage = function(code, image, imageType) {
    return new Promise(function(resolve, reject) {
        let result = {
            "result": "OK",
            "message": ""
        }

        resolve(result);
    });
};

data.getUsers = function() {
    return new Promise(function(resolve, reject) {
        resolve(createUser("image/jpeg", "jim test", "ABC123"));
    });
};

data.updateImage = function(code, status, comment) {
    return new Promise(function(resolve, reject) {
        let result = {
            "result": "OK",
            "message": ""
        }

        resolve(result);
    });
};
