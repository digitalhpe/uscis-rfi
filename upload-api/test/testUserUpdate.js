"use strict";

var should = require('should');
var request = require('supertest');
var app = require('../app');
const { expect } = require('chai');

describe('testUserUpdate', function () {
    it ('should return 400 for missing status', function() {
        request(app).post('/api/user/image/update/ABC100')
                    .send({"comment": "update with API"})
                    .end(function(err, res) {
                        expect(res.status).to.equal(400);
                    });
    });

    it ('should return 400 for missing comment', function() {
        request(app).post('/api/user/image/update/ABC100')
                    .send({"status": "approved"})
                    .end(function(err, res) {
                        expect(res.status).to.equal(400);
                    });
    });

    it ('should return 200 for a valid call', function() {
        request(app).post('/api/user/image/update/ABC100')
                    .send({"status": "approved", "comment": "update with API"})
                    .end(function(err, res) {
                        expect(res.status).to.equal(200);
                    });
    });
});