const server = require('../bin/www');
const puppeteer = require('puppeteer');
const { expect } = require('chai');

let browser;

describe('sample test', function () {
    let page;

    before (async function () {
        browser = await puppeteer.launch({ headless: true });
        page = await browser.newPage();
        await page.goto('http://localhost:8181');
    });

    it('should have the correct page title', async function () {
        expect(await page.title()).to.eql('USCIS Client Site');
    });

    it('should have disabled submit button', async function () {
        const disabled = await page.evaluate(() => document.getElementById('submitImage').getAttribute('disabled'));
        expect(disabled).to.equal('');
    });

    it('should have empty code textbox', async function () {
        const code = await page.evaluate(() => document.getElementById('code').value);
        expect(code).to.equal('');
    });

    it('should have enabled submit button', async function () {
        await page.evaluate(() => imageSelected = true);
        await page.type("#code", "ABC100");

        const disabled = await page.evaluate(() => document.getElementById('submitImage').getAttribute('disabled'));
        expect(disabled).to.equal(null);
    });

    it('should display upload successful', async function () {
        await page.click('#submitImage');

        const dialogTitle = await page.evaluate(() => document.getElementById('dialogTitle').innerHTML);
        expect(dialogTitle).to.equal('Image Submission Successful');
    });

    after (async function () {
        await page.close();
        browser.close();
        server.close();
    });
});
