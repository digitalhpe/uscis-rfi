"use strict";

var should = require('should');
var request = require('supertest');
var app = require('../app');
const { expect } = require('chai');

describe('testUserAdd', function () {
    it ('should return 400 for missing image', function() {
        request(app).post('/api/user/image/add/ABC100')
                    .send({"imageType": "image/jpeg"})
                    .end(function(err, res) {
                        expect(res.status).to.equal(400);
                });
    });

    it ('should return 400 for missing imageType', function() {
        request(app).post('/api/user/image/add/ABC100')
                    .send({"image": "base64 encoded image string"})
                    .end(function(err, res) {
                        expect(res.status).to.equal(400);
                    });
    });

    it ('should return 200 for a valid call', function() {
        request(app).post('/api/user/image/add/ABC100')
                    .send({"image": "base64 encoded image string", "imageType": "image/jpeg"})
                    .end(function(err, res) {
                        expect(res.status).to.equal(200);
                    });
    });
});