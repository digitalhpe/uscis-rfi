"use strict";

let express = require('express');
let path = require('path');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let log = require('./data/logger');

const auth = require("./data/auth.js")();

let routes = require('./routes/index');
let user = require('./routes/user');
let adminUser = require('./routes/adminUser');
let token = require('./routes/token');
let ping = require('./routes/hello');

let testConfig = false;
if (process.env.NODE_ENV === 'test') {
    testConfig = true;
    log.info('Unit Test Mode Enabled');
} else {
    log.info('Normal Mode');
}

let dataLayer;

//dataLayer = require('./data/data');

if (testConfig) {
    dataLayer = require('./test/mockData');
} else {
    dataLayer = require('./data/data');
}

// Middleware error handler for json response
function handleError(err, req, res, next) { // NOSONAR
    let output = {
        result: "Error",
        message: err.message
    };
    let statusCode = err.status || 500;
    res.status(statusCode).json(output);
}

let app = express();

app.locals.dataLayer = dataLayer;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

if (process.env.NODE_ENV !== 'test') {
    app.use(morgan('dev'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true, limit: '20mb'}));
app.use(express.static(path.join(__dirname, 'public')));

// Enable CORS from client-side
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

app.use(auth.initialize());

app.use('/', routes);
app.use('/api/user', user);
app.use('/api/admin', adminUser);
app.use('/api/token', token);
app.use('/hello', ping);

// error handling middleware last
app.use([
    handleError
]);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) { // NOSONAR
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) { // NOSONAR
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
