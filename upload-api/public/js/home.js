var imageSelected = false;

$(function() {

    var MAX_UPLOAD_SIZE = 7 * 1024 * 1000;
    var useWebCam = $('#useWebCam');
    var imagePreview = $('#imagePreview');
    var videoCapture = $('#videoCapture');
    var takePicture = $('#takePicture');
    var retakePicture = $('#retakePicture');
    var submitImage = $('#submitImage');
    var code = $('#code');
    var canvas;

    if (isIos()) {
        useWebCam.hide();
        $('#imageUploadParentTile').removeClass("is-vertical");
        $('#tileContent').removeClass("tile");
    }

    // default layout to image upload preview mode
    setPreviewMode(true);

    // set submit button to disabled initially
    submitImage.prop('disabled', true);

    takePicture.on('click', function() {
        takeSnapshot();
        takePicture.hide();
        retakePicture.show();
    });

    retakePicture.on('click', function() {
        imageSelected = false;
        startWebCam();
        validateRequiredItems();
    });

    code.on('input', function() {
        validateRequiredItems();
    });

    $("#uploadImage").change(function(e) {

        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

            var file = e.originalEvent.srcElement.files[i];

            if (file.size <= MAX_UPLOAD_SIZE) {
                var reader = new FileReader();
                reader.onloadend = function () {
                    imagePreview.attr('src', reader.result);
                    imageSelected = true;
                    setPreviewMode(true);
                    validateRequiredItems();
                    stopWebCam();
                };
                reader.readAsDataURL(file);
            } else {
                displayDialog('Image File Too Large', 'The image file you selected is too large. Please use an image that is less than 5 MB.');
                $('#modalFileTooLarge').addClass('is-active');
            }
        }
    });

    $(".button-close").click(function() {
        $(".modal").removeClass("is-active");
    });

    useWebCam.click(function() {
        startWebCam();
    });

    submitImage.click(function() {
        var img = imagePreview.attr("src");
        var contentType = getImageType(img);
        var imgStripped = img.substring(img.indexOf(',') + 1);
        uploadImage(code.val(), contentType, imgStripped);
    });

    /**
     * turn on web cam and start preview once permissions are granted
     */
    function startWebCam() {
        var video = document.getElementById('videoCapture');

        if (navigator.mediaDevices) {
            // access the web cam
            navigator.mediaDevices.getUserMedia({video: true})
            // permission granted:
                .then(function(stream) {
                    video.srcObject = stream;
                    setPreviewMode(false);
                    imageSelected = false;
                    validateRequiredItems();
                })
                // permission denied:
                .catch(function(error) {
                    displayDialog('Error Accessing Webcam', 'An error has occurred while trying to access your webcam.');
                });
        }
    }

    /**
     * stop the web cam
     */
    function stopWebCam() {
        var video = document.getElementById('videoCapture');
        if (video.srcObject !== undefined && video.srcObject !== null) {
            var track = video.srcObject.getTracks()[0];
            track.stop();
        }
    }

    /**
     *  generates a still frame image from the stream in the <video>
     *  appends the image to the <body>
     */
    function takeSnapshot() {
        var video = document.getElementById('videoCapture');
        var context;
        var width = video.offsetWidth
            , height = video.offsetHeight;

        canvas = canvas || document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;

        context = canvas.getContext('2d');
        context.drawImage(video, 0, 0, width, height);

        imagePreview.attr('src', canvas.toDataURL('image/png'));
        imagePreview.show();
        videoCapture.hide();
        imageSelected = true;
        stopWebCam();
        validateRequiredItems();
    }

    /**
     * Used to toggle between image upload mode and web cam snapshot mode
     * @param showImagePreview Sets the preview mode. true = upload mode, false = web cam mode
     */
    function setPreviewMode(showImagePreview) {
        if (showImagePreview) {
            imagePreview.show();
            videoCapture.hide();
            retakePicture.hide();
            takePicture.hide();
        } else {
            imagePreview.hide();
            videoCapture.show();
            retakePicture.hide();
            takePicture.show();
        }
    }

    /**
     * Gets the image type from the base64 encoded image
     * @param image The base64 encoded image
     * @returns {string} The image type (image/jpeg, image/png, etc)
     */
    function getImageType(image) {
        return image.substring(5, image.indexOf(';'));
    }

    /**
     * Uploads the image via web service call
     * @param imageCode Image upload code
     * @param imageType Image type (image/jpeg, image/png, etc)
     * @param image Base64 encoded image
     */
    function uploadImage(imageCode, imageType, image) {
        $('#submitImage').addClass('is-loading');

        $.post('api/user/image/add/' + imageCode, {
                imageType: imageType,
                image: image
            }).done(function() {
                stopWebCam();
                retakePicture.hide();
                submitImage.removeClass('is-loading');
                code.val('');
                imagePreview.attr('src', '/images/preview_placeholder.png');
                validateRequiredItems();
                displayDialog('Image Submission Successful', 'Thank you for submitting your image.');
            }).fail(function(jqXHR, textStatus, errorThrown) {
                switch (jqXHR.status) {
                    case 404:
                        submitImage.removeClass('is-loading');
                        displayDialog('Code Not Found', 'The code you have entered was not found. Please enter a valid code and try again.');
                        break;
                    case 409:
                        submitImage.removeClass('is-loading');
                        displayDialog('Code Already Used', 'The code you have entered has already been used. Please try again with a different code.');
                        break;
                    default:
                        submitImage.removeClass('is-loading');
                        displayDialog('Error Uploading Image', 'An error has occurred while uploading your image. Please try again.');
                }
            });
    }

    /**
     * Validates required fields and enables / disables submit button
     */
    function validateRequiredItems() {
        if (code.val().trim() && imageSelected) {
            submitImage.prop('disabled', false);
        } else {
            submitImage.prop('disabled', true);
        }
    }


    function isIos() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        return ( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) );
    }

    /**
     * Helper function to display modal dialog
     * @param title Message title
     * @param message Message body
     */
    function displayDialog(title, message) {
        $('#dialogTitle').html(title);
        $('#dialogMessage').html(message);
        $('#modalDialog').addClass('is-active');
    }
});