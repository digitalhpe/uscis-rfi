# upload-api

> Image upload site for DHS RFI demo

## Build Setup

``` bash
# install dependencies
npm install

# setup and populate mongo db
mongo upload-api/data/dbsetup.js

# run web services and website at localhost:8181
npm start

# run all tests
npm test
```

Web services are accessible at: localhost:8181/api
