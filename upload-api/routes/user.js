"use strict";

const express = require('express');
const router = express.Router();
const utils = require('./utils');
const auth = require("../data/auth")();

router.get('/all', auth.authenticate(), function(req, res) {
    console.log("get all");
    const db = req.app.locals.dataLayer;

    let result = {};
    result.result = "OK";
    result.message = "";

    Promise.resolve(db.getUsers())
        .then(
            function(data) {
                result.data = data;
                res.status(200).json(result);
            }
        ).catch(
        function(err) {
            err.url = req.baseUrl;
            utils.handleError(err, res);
        }
    );
});

router.get('/pending', auth.authenticate(), function(req, res) {
    console.log("pending");
    const db = req.app.locals.dataLayer;

    let result = {};
    result.result = "OK";
    result.message = "";

    Promise.resolve(db.getPendingUsers())
        .then(
            function(data) {
                result.data = data;
                res.status(200).json(result);
            }
        ).catch(
        function(err) {
            err.url = req.baseUrl;
            utils.handleError(err, res);
        }
    );
});

router.get('/reset', auth.authenticate(), function(req, res) {
    console.log("reset");
    const db = req.app.locals.dataLayer;

    let result = {};
    result.result = "OK";
    result.message = "";

    Promise.resolve(db.resetUsers())
        .then(
            function(data) {
                result.data = data;
                res.status(200).json(result);
            }
        ).catch(
        function(err) {
            err.url = req.baseUrl;
            utils.handleError(err, res);
        }
    );
});

router.get('/:code', auth.authenticate(), function(req, res) {
    console.log("get one");

    const db = req.app.locals.dataLayer;

    let result = {};
    result.result = "OK";
    result.message = "";

    let usercode = req.params.code.toUpperCase();
    console.log(usercode);

    Promise.resolve(db.getUser(usercode))
        .then(
            function(data) {
                if (data === null || data.length === 0) {
                    let err = {};
                    err.message = "Not Found";
                    utils.handleError(err, res);
                } else {
                    result.data = data;
                    res.status(200).json(result);
                }
            }
        ).catch(
            function(err) {
                err.url = req.baseUrl;
                err.message = "Not Found";
                utils.handleError(err, res);
            }
        );
});

router.post('/image/add/:code', function(req, res) {
    let db = req.app.locals.dataLayer;

    let result = {};
    result.result = "OK";
    result.message = "";

    let code = req.params.code.toUpperCase();
    let image = req.body.image;
    let imageType = req.body.imageType;

    if (image === undefined) {
        res.status(400).json({"result": "error", "message": "image missing"});
        return;
    }

    if (imageType === undefined) {
        res.status(400).json({"result": "error", "message": "imageType missing"});
        return;
    }

    Promise.resolve(db.addImage(code, image, imageType))
        .then(
            function() {
                res.status(200).json(result);
            }
        ).catch(
            function(err) {
                err.url = req.baseUrl;
                utils.handleError(err, res);
            }
        );
});

router.post('/image/update/:code', function(req, res) {
    let db = req.app.locals.dataLayer;

    let result = {};
    result.result = "OK";
    result.message = "";

    let code = req.params.code.toUpperCase();
    let status = req.body.status;
    let comment = req.body.comment;

    if (status === undefined) {
        res.status(400).json({"result": "error", "message": "status missing"});
        return;
    }

    if (comment === undefined) {
        res.status(400).json({"result": "error", "message": "comment missing"});
        return;
    }

    Promise.resolve(db.updateImage(code, status, comment))
        .then(
            function() {
                res.status(200).json(result);
            }
        ).catch(
        function(err) {
            err.url = req.baseUrl;
            utils.handleError(err, res);
        }
    );
});

module.exports = router;