"use strict";

let express = require('express');

let router = express.Router();


router.get('/', function(req, res, next) { // NOSONAR

    let result = {};
    result.hello = "Bazinga!";
    res.status(200).json(result);

});

module.exports = router;