"use strict";

const express = require('express');
const router = express.Router();
const jwt = require("jwt-simple");
const config = require('config');
const logger = require('../data/logger');

router.post('/', function(req, res) {
    let db = req.app.locals.dataLayer;

    let result = {};
    result.result = "OK";
    result.message = "";

    if (req.body.username && req.body.password) {
        const username = req.body.username;
        const password = req.body.password;
        db.getAdminUser(username, password)
            .then(function (user) {
                user.comparePassword(password, function(err, isMatch) {
                    if(err || !isMatch) {
                        res.sendStatus(401);
                    } else if (isMatch) {
                        const payload = {
                            id: user.username,
                            exp: ((Date.now() / 1000) + 7200)
                        };
                        result.token = jwt.encode(payload, config.get('secret'));
                        res.status(200).json(result);
                    }
                });
            })
            .catch(function (err){
                logger.error(err);
                res.sendStatus(401);
            })
    } else {
        res.sendStatus(401);
    }
});

module.exports = router;