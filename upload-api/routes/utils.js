"use strict";

let logger = require('../data/logger');

module.exports = {
    handleError: function(err, res) {
        if (err) {

            logger.error(err);
            if (err.message === 'Not Found'){
                res.status(404).json({"result": "error", "message": err.message});
            } else if (err.message === 'Server error') {
                res.status(500).json({"result": "error", "message": err.message});
            } else if (err.message === 'Invalid token') {
                res.status(401).json({"result": "error", "message": err.message});
            } else if (err.message === 'User Not Authorized') {
                res.status(403).json({"result": "error", "message": err.message});
            } else if (err.message === 'User does not exist') {
                res.status(401).json({"result": "error", "message": err.message});
            } else if (err.message === 'Invalid password') {
                res.status(401).json({"result": "error", "message": err.message});
            } else if (err.message === 'Image already uploaded') {
                res.status(409).json({"result": "error", "message": err.message});
            } else if (err.code || err.reason) {
                res.status(500).json({"result": "error", "code": err.code, "message": err.message});
            } else {
                res.status(500).json({"result": "error", "message": err});
            }
        }
    }
};