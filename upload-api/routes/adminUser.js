"use strict";

const express = require('express');
const router = express.Router();
const utils = require('./utils');
let auth = require("../data/auth")();

router.post('/', auth.authenticate(), function(req, res) {
    console.log("create admin");
    const db = req.app.locals.dataLayer;

    const result = {};
    result.result = "OK";
    result.message = "";

    const username = req.body.username;
    const password = req.body.password;

    Promise.resolve(db.createAdminUser(username, password))
        .then(
            function() {
                res.status(200).json(result);
            }
        ).catch(
            function(err) {
                err.url = req.baseUrl;
                utils.handleError(err, res);
            }
        );
});

module.exports = router;