"use strict";

let express = require('express');
let router = express.Router();

// main login page //
router.get('/', function(req, res){
    res.render('home', {title: 'USCIS Client Site'});
});

module.exports = router;
