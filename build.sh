#!/bin/bash

## Generic master build script that chains together other scripts to build this app.
## other scripts needed:
## * compile.sh - should compile and package the application
## * deploy.sh - generic script that deploys the app to a docker container

echo "Building USCIS RFI Demonstration app"

./compile.sh

if [ $? -eq 0 ]
then
	./test.sh
fi

if [ $? -eq 0 ]
then
	./deploy.sh
	exit $?
fi
