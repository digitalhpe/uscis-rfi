# admin-web

Administrative site for DHS RFI demo. Users can log into the site and approve or deny images submitted by users of the Upload/API web site.

This site is built with [Vue.js](https://vuejs.org/) and is a single-page web application. There is no server-side component. The app calls into the API exposed from the upload-api project in this same repository.

The layout of the site uses the [Bulma](https://bulma.io/) framework and is responsive.

The site is bundled and packaged using [WebPack](https://webpack.js.org/).
## Build and Test Locally

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
