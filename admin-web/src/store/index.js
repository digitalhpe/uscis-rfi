import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
  key: 'dhprfi',
  storage: window.localStorage
})

const store = new Vuex.Store({
  state: {
    token: ''
  },
  getters: {
    token: state => state.token
  },
  mutations: {
    addToken (state, token) {
      state.token = token
    }
  },
  plugins: [vuexLocalStorage.plugin]
})

export default store
