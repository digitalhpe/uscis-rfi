import 'es6-promise/auto'
import Vue from 'vue'
import VeeValidate from 'vee-validate'
import LoginComponent from '@/components/Login'
import moxios from 'moxios'
import flushPromises from 'flush-promises'

Vue.use(VeeValidate)

describe('Login.vue', () => {
  beforeEach(() => {
    moxios.install()
  })
  afterEach(() => {
    moxios.uninstall()
  })

  it('Login missing username', async () => {
    const Constructor = Vue.extend(LoginComponent)
    const login = new Constructor().$mount()
    login.password = 'ssw'

    const button = login.$el.querySelector('.button')
    const clickEvent = new window.Event('click')
    button.dispatchEvent(clickEvent)
    login._watcher.run()
    const userError = login.$el.querySelector('#userNameError')

    await flushPromises()
    console.log(userError)
    expect(login.$el.textContent).to.contain('The username field is required.')
  })

  it('Login missing password', async () => {
    const Constructor = Vue.extend(LoginComponent)
    const login = new Constructor().$mount()
    login.username = 'ssw'

    const button = login.$el.querySelector('.button')
    const clickEvent = new window.Event('click')
    button.dispatchEvent(clickEvent)
    login._watcher.run()

    await flushPromises()
    expect(login.$el.textContent).to.contain('The password field is required.')
  })

  it('Login fails due to bad credentials', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/token', { status: 401 })

    const Constructor = Vue.extend(LoginComponent)
    const login = new Constructor().$mount()
    login.username = 'trux'
    login.password = 'password'

    const button = login.$el.querySelector('.button')
    button.click()

    moxios.wait(function () {
      console.log('Done waiting')
      expect(login.invalid).equals(true)
      done()
    })
  })
})
