import { mount, createLocalVue } from 'vue-test-utils'

import StatusComponent from '@/components/Status'
import moxios from 'moxios'
import VeeValidate from 'vee-validate'
import $store from '../../../src/store'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(VeeValidate)
localVue.use(VueRouter)

function getStatusRecords () {
  const result = {}
  result.data = []
  const item3 = {}
  item3.firstName = 'Jane'
  item3.lastName = 'Doe'
  item3.comments = 'This was particularly useful example'
  item3.status = 'Approved'
  item3.code = '000111222333444'
  result.data.push(item3)

  const item4 = {}
  item4.firstName = 'William'
  item4.lastName = 'Butler'
  item4.comments = 'This was a horrible example of what to do here'
  item4.status = 'Denied'
  item4.code = '0004445556666'
  result.data.push(item4)

  return result
}

describe('Status.vue', () => {
  beforeEach(() => {
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('All Data loaded', (done) => {
    const mockResponse = getStatusRecords()
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      expect(3).equals(vm.$el.querySelectorAll('.dhs-status tr').length)
      done()
    })
  })

  it('Search Name field found a record', (done) => {
    const mockResponse = getStatusRecords()
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('.dhs-searchterm')
      commentField.element.value = 'William'
      commentField.trigger('input')
      wrapper.setData({ searchField: 'Name' })

      const button = wrapper.find('#button-holder')
      button.trigger('click')

      localVue.nextTick()
        .then(() => {
          expect(1).equals(vm.status.length)
          done()
        })
    })
  })

  it('Search Name field found no records', (done) => {
    const mockResponse = getStatusRecords()
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('.dhs-searchterm')
      commentField.element.value = 'Natalie'
      commentField.trigger('input')
      wrapper.setData({ searchField: 'Name' })

      const button = wrapper.find('#button-holder')
      button.trigger('click')

      localVue.nextTick()
        .then(() => {
          expect(0).equals(vm.status.length)
          expect('No matches found').equals(vm.statusMessage)
          done()
        })
    })
  })

  it('Search Status field found a record', (done) => {
    const mockResponse = getStatusRecords()
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('.dhs-searchterm')
      commentField.element.value = '000111222333444'
      commentField.trigger('input')
      wrapper.setData({ searchField: 'Code' })

      const button = wrapper.find('#button-holder')
      button.trigger('click')

      localVue.nextTick()
        .then(() => {
          expect(1).equals(vm.status.length)
          done()
        })
    })
  })

  it('Search Status field found a record', (done) => {
    const mockResponse = getStatusRecords()
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('.dhs-searchterm')
      commentField.element.value = 'Denied'
      commentField.trigger('input')
      wrapper.setData({ searchField: 'Status' })

      const button = wrapper.find('#button-holder')
      button.trigger('click')

      localVue.nextTick()
        .then(() => {
          expect(1).equals(vm.status.length)
          done()
        })
    })
  })

  it('Search Comments field found records', (done) => {
    const mockResponse = getStatusRecords()
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('.dhs-searchterm')
      commentField.element.value = 'example'
      commentField.trigger('input')
      wrapper.setData({ searchField: 'Comments' })

      const button = wrapper.find('#button-holder')
      button.trigger('click')

      localVue.nextTick()
        .then(() => {
          expect(2).equals(vm.status.length)
          done()
        })
    })
  })

  it('Search fails no records to search', (done) => {
    const mockResponse = {data: []}
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('.dhs-searchterm')
      commentField.element.value = 'example'
      commentField.trigger('input')
      wrapper.setData({ searchField: 'Comments' })

      const button = wrapper.find('#button-holder')
      button.trigger('click')

      localVue.nextTick()
        .then(() => {
          expect('No data available to search').equals(vm.statusMessage)
          done()
        })
    })
  })

  it('Data Reset Succeeds', (done) => {
    const mockResponse = getStatusRecords()
    const response = JSON.stringify(mockResponse)
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/all', {
      status: 200,
      responseText: response
    })

    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/reset', {
      status: 200
    })

    const wrapper = mount(StatusComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const button = vm.$el.querySelector('#resetData')
      console.log(button)
      button.click()
      localVue.nextTick()
        .then(() => {
          const modalbutton = vm.$el.querySelector('#acceptReset')
          modalbutton.click()
          moxios.wait(() => {
            // The component actually errors out trying to route but this is to important to the test
            done()
          })
        })
    })
  })
})
