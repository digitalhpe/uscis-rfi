import { mount, createLocalVue } from 'vue-test-utils'

import UploadComponent from '@/components/Upload'
import moxios from 'moxios'
import VeeValidate from 'vee-validate'
import flushPromises from 'flush-promises'
import $store from '../../../src/store'

const localVue = createLocalVue()
localVue.use(VeeValidate)

describe('Upload.vue', () => {
  beforeEach(() => {
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Data loaded', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/pending', {
      status: 200,
      responseText: '{ "data": [{"image": "", "firstName": "Jane", "lastName": "Doe", "address": "1836 Par Drive, Los Angeles, CA 90013", "dob": "1/2/1983", "code": "000111222333"}, { "image": "", "firstName": "William", "lastName": "Butler", "address": "3640 Aaron Smith Drive, Harrisburg, PA 17111",  "dob": "6/3/1953",  "code": "000444222555" }]}'
    })

    const wrapper = mount(UploadComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(function () {
      expect(2).equals(vm.$el.querySelectorAll('.dhs-name').length)
      done()
    })
  })

  it('No Data loaded', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/pending', {
      status: 404
    })
    const wrapper = mount(UploadComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(function () {
      expect(1).equals(vm.$el.querySelectorAll('.dhs-norecords').length)
      done()
    })
  })
  it('Approval validation failed', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/pending', {
      status: 200,
      responseText: '{ "data": [{"image": "", "firstName": "Jane", "lastName": "Doe", "address": "1836 Par Drive, Los Angeles, CA 90013", "dob": "1/2/1983", "code": "000111222333"}, { "image": "", "firstName": "William", "lastName": "Butler", "address": "3640 Aaron Smith Drive, Harrisburg, PA 17111",  "dob": "6/3/1953",  "code": "000444222555" }]}'
    })
    const wrapper = mount(UploadComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$mount()
    vm.$store.commit('addToken', '12345-Token')
    // await flushPromises()
    moxios.wait(function () {
      console.log('loaded')
      const button = vm.$el.querySelector('#approve0')
      console.log(button)
      button.click()
      localVue.nextTick()
        .then(() => {
          console.log(vm.$el.querySelector('#error0'))
          expect('').equals(vm.$el.querySelector('#error0').getAttribute('style'))
          done()
        })
    })
  })

  it('Denial validation failed', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/pending', {
      status: 200,
      responseText: '{ "data": [{"image": "", "firstName": "Jane", "lastName": "Doe", "address": "1836 Par Drive, Los Angeles, CA 90013", "dob": "1/2/1983", "code": "000111222333"}, { "image": "", "firstName": "William", "lastName": "Butler", "address": "3640 Aaron Smith Drive, Harrisburg, PA 17111",  "dob": "6/3/1953",  "code": "000444222555" }]}'
    })
    const wrapper = mount(UploadComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$mount()
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(async function () {
      await flushPromises()
      const button = vm.$el.querySelector('#deny0')
      button.click()
      localVue.nextTick()
        .then(() => {
          expect('').equals(vm.$el.querySelectorAll('#error0').item(0).getAttribute('style'))
          done()
        })
    })
  })

  it('Approval succeeds', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/pending', {
      status: 200,
      responseText: '{ "data": [{"image": "", "firstName": "Jane", "lastName": "Doe", "address": "1836 Par Drive, Los Angeles, CA 90013", "comment": "This is a comment", "dob": "1/2/1983", "code": "000111222333"}, { "image": "", "firstName": "William", "lastName": "Butler", "address": "3640 Aaron Smith Drive, Harrisburg, PA 17111", "comment": "This is a comment",  "dob": "6/3/1953",  "code": "000444222555" }]}'
    })

    moxios.stubRequest('https://dhsupload.digitaldxc.com/api/user/image/update/000111222333', {
      status: 200
    })

    const wrapper = mount(UploadComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$mount()
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('#comments0')
      commentField.element.value = 'Test text'
      commentField.trigger('input')

      const button = vm.$el.querySelector('#approve0')
      button.click()
      localVue.nextTick()
        .then(() => {
          console.log(vm.$el.querySelector('#error0'))
          expect('display: none;').equals(vm.$el.querySelector('#error0').getAttribute('style'))
          const modalButton = vm.$el.querySelector('.dhp-approve-button')
          console.log(modalButton)
          modalButton.click()
          moxios.wait(() => {
            done()
          })
        })
    })
  })

  it('Deny succeeds', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/pending', {
      status: 200,
      responseText: '{ "data": [{"image": "", "firstName": "Jane", "lastName": "Doe", "address": "1836 Par Drive, Los Angeles, CA 90013", "comment": "This is a comment", "dob": "1/2/1983", "code": "000111222333"}, { "image": "", "firstName": "William", "lastName": "Butler", "address": "3640 Aaron Smith Drive, Harrisburg, PA 17111", "comment": "This is a comment",  "dob": "6/3/1953",  "code": "000444222555" }]}'
    })

    moxios.stubRequest('https://dhsupload.digitaldxc.com/api/user/image/update/000111222333', {
      status: 200
    })

    const wrapper = mount(UploadComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$mount()
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('#comments0')
      commentField.element.value = 'Test text'
      commentField.trigger('input')

      const button = vm.$el.querySelector('#deny0')
      button.click()
      localVue.nextTick()
        .then(() => {
          console.log(vm.$el.querySelector('#error0'))
          expect('display: none;').equals(vm.$el.querySelector('#error0').getAttribute('style'))
          const modalButton = vm.$el.querySelector('.dhp-approve-button')
          console.log(modalButton)
          modalButton.click()
          moxios.wait(() => {
            done()
          })
        })
    })
  })

  it('Approval fails', (done) => {
    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/pending', {
      status: 200,
      responseText: '{ "data": [{"image": "", "firstName": "Jane", "lastName": "Doe", "address": "1836 Par Drive, Los Angeles, CA 90013", "comment": "This is a comment", "dob": "1/2/1983", "code": "000111222333"}, { "image": "", "firstName": "William", "lastName": "Butler", "address": "3640 Aaron Smith Drive, Harrisburg, PA 17111", "comment": "This is a comment",  "dob": "6/3/1953",  "code": "000444222555" }]}'
    })

    moxios.stubRequest('https://uscisupload.digitaldxc.com/api/user/image/update/000111222333', {
      status: 500
    })

    const wrapper = mount(UploadComponent, {
      mocks: {
        $store
      }
    })
    const vm = wrapper.vm
    vm.$mount()
    vm.$store.commit('addToken', '12345-Token')

    moxios.wait(() => {
      const commentField = wrapper.find('#comments0')
      commentField.element.value = 'Test text'
      commentField.trigger('input')

      const button = vm.$el.querySelector('#approve0')
      button.click()
      localVue.nextTick()
        .then(() => {
          expect('display: none;').equals(vm.$el.querySelector('#error0').getAttribute('style'))
          const modalButton = vm.$el.querySelector('.dhp-approve-button')
          modalButton.click()
          moxios.wait(() => {
            const modalError = wrapper.find('#errorModal')
            expect(modalError.classes()).contain('is-active')
            done()
          })
        })
    })
  })
})
