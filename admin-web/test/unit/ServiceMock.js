import Vue from 'vue'

const services = {}

Vue.mixin({
  beforeCreate () {
    const options = this.$options
    if (options.services) {
      this.$services = options.services
    } else if (options.parent && options.parent.$services) {
      this.$services = options.parent.$services
    }
  }
})

services.updateApproval = function (token, code, approved, comments) {
  return new Promise(function (resolve, reject) {
    resolve()
  })
}

services.getUploads = function (token) {
  return new Promise(function (resolve, reject) {
    const result = []
    const item1 = {}
    item1.image = ''
    item1.name = 'Jane Doe'
    item1.address = '1836 Par Drive, Los Angeles, CA 90013'
    item1.dob = '1/2/1983'
    item1.code = '000111222333'
    result.push(item1)

    const item2 = {}
    item2.image = ''
    item2.name = 'William P. Butler'
    item2.address = '3640 Aaron Smith Drive, Harrisburg, PA 17111'
    item2.dob = '6/3/1953'
    item2.code = '0004445556666'
    result.push(item2)
    resolve(result)
  })
}

services.searchStatus = function (token, field, term) {
  return new Promise(function (resolve, reject) {
    const result = []
    const item3 = {}
    item3.name = 'Jane Doe'
    item3.comments = 'This was particularly useful'
    item3.status = 'Approved'
    item3.code = '000111222333'
    result.push(item3)

    const item4 = {}
    item4.name = 'William P. Butler'
    item4.comments = 'This was a horrible example of what to do here'
    item4.status = 'Denied'
    item4.code = '0004445556666'
    result.push(item4)
    resolve(result)
  })
}

services.getStatus = function (token) {
  return new Promise(function (resolve, reject) {
    const result = []
    const item1 = {}
    item1.name = 'Jane Doe'
    item1.comments = 'This was particularly useful'
    item1.status = 'Approved'
    item1.code = '000111222333'
    result.push(item1)

    const item2 = {}
    item2.name = 'William P. Butler'
    item2.comments = 'This was a horrible example of what to do here'
    item2.status = 'Denied'
    item2.code = '0004445556666'
    result.push(item2)

    const item3 = {}
    item3.name = 'Jane Doe'
    item3.comments = 'This was particularly useful'
    item3.status = 'Approved'
    item3.code = '000111222333'
    result.push(item3)

    const item4 = {}
    item4.name = 'William P. Butler'
    item4.comments = 'This was a horrible example of what to do here'
    item4.status = 'Denied'
    item4.code = '0004445556666'
    result.push(item4)
    resolve(result)
  })
}

services.getToken = function (username, password) {
  const body = {}
  body.username = username
  body.password = password
  return new Promise(function (resolve, reject) {
    if (body.username === 'good') {
      resolve('faketoken')
    } else {
      reject(reject(new Error('Invalid Login')))
    }
  })
}

export default services
