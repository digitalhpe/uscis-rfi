#!/bin/bash

## Compile and package steps only belong in this script.
## The script must return proper return codes so that when chained with other steps
## in the build lifecycle, other scripts in the sequence will know whether to continue to process or not.

echo "Compiling/Packaging Admin Web Site"

cd admin-web

npm install
npm run build

if [ $? -eq 0 ]
then
	echo "Successfully compiled and packaged the app."
	exit 0
fi

echo "Compiling/Packaging Upload and API Site"

cd ../upload-api/web

npm install

cd ..

if [ $? -eq 0 ]
then
	echo "Successfully compiled and packaged the app."
	exit 0
else
	echo "Errors encountered while compiling and packaging the app."
	exit 1
fi
